return {
  keyboard = {
    up = "w",
    down = "s",
    left = "a",
    right = "d",
    action = "return",
    attack = "right shift",
    item_1 = "q",
    item_2 = "e",
    pause = "escape",
  },

  joypad = {
    up = "axis 1 -",
    down = "axis 1 +",
    left = "axis 0 -",
    right = "axis 0 +",
    action = "button 0",
    attack = "button 5",
    pause = "button 7",
    item_1 = "button 2",
    item_2 = "button 3",
  },

  keyboard_solarus = {
    up = "up",
    down = "down",
    left = "left",
    right = "right",
    action = "space",
    attack = "c",
    pause = "d",
    item_1 = "x",
    item_2 = "v",
  },
}